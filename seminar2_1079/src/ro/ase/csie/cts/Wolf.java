package ro.ase.csie.cts;

public class Wolf implements Runner{

	static final int WOLF_SPEED = 10;
	
	public Wolf() {}
	
	@Override
	public int getSpeed() {
		return WOLF_SPEED;
	}

	@Override
	public String toString() {
		return "Wolf [WOLF_SPEED=" + WOLF_SPEED + "]";
	}

}
