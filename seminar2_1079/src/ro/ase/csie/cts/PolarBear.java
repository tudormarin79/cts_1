package ro.ase.csie.cts;

public class PolarBear extends Bear{

	static final int POLAR_BEAR_SPEED = 5;
	@Override
	public int getSpeed() {
		return POLAR_BEAR_SPEED;
	}
	@Override
	public String toString() {
		return "PolarBear []";
	}

	
}
