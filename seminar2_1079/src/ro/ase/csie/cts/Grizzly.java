package ro.ase.csie.cts;

public class Grizzly extends Bear{

	static final int GRIZZLY_BEAR = 25;
	
	public Grizzly() {
		
	}
	
	@Override
	public int getSpeed() {
		return GRIZZLY_BEAR;
	}

	@Override
	public String toString() {
		return "Grizzly []";
	}
	

}
