package ro.ase.csie.cts;

public class Human implements Runner{

	int age;
	
	public Human() {
		age = 21;
	}
	
	@Override
	public int getSpeed() {
		return 0;
	}

	public int getAge() {
		if(age < 50)
		return 10;
		else
			return 7;
	}

	public void setAge(int age) {
		this.age = age;
	}

}
